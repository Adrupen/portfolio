
function [ V ] = BBHE( Vbf )
%BBHE Brightness Preserving Bi Histogram Equalization
%   1. Divides the image into two sub-images based on the mean value as a
%   threshold. 
%   2. Then computes two separate histogram equalizations. 
%   3. Sub images are then recombined. This will ensure that the brightness 
%   mean is better perserved.

    [M, N] = size(Vbf);
    t = mean(mean(Vbf));
    subImages = zeros(M,N,2);
    subImages = subImages(:,:,:)-1;
    sizeSub = zeros(2,1);
    % Divide images based on t, empty pixels are represented by -1
    for m = 1:M
        for n = 1:N
            if Vbf(m,n) <= t
                subImages(m,n,1) = Vbf(m,n);
                sizeSub(1) = sizeSub(1)+1;
            else
                subImages(m,n,2) = Vbf(m,n);
                sizeSub(2) = sizeSub(2)+1;
            end
        end
    end
    
    V = Vbf;
    lookup = zeros(256,1);
    start = 1;
    range = round(t*256);
    
    for i = 1:2
        h = zeros(256,1);
        d = 1/sizeSub(i);
        %calcualte histogram for sub-image i
        for m = 1:M
            for n = 1:N
                if subImages(m,n,i) > -1
                    h((subImages(m,n,i)*255)+1) = h((subImages(m,n,i)*255)+1)+d;
                end
            end
        end
        %Calculate cumulative sum for the given range of sub-image i
        s = 0.0;
        for j = start:range
            s = s+h(j);
            lookup(j) = (s*(range-start))+start;
        end
        %Convert old values to new ones by lookup table
        for m = 1:M
            for n = 1:N
                if subImages(m,n,i) > -1
                    V(m,n) = (lookup((V(m,n)*255)+1))/256;
                end
            end
        end
        start = range;
        range = 256;
        
    end
end   



