
function [ V ] = vTransform( Vbf, s )
%VTRANSFORM
%   1. Put all intensity values into an array
%   2. Iterate through given segment count and evaluate the linear function
%   then translate old values into new ones by the linear function. Values
%   are stored in a lookup table.
%   3. Get the new picture by running old values though the lookup table.

    [M, N] = size(Vbf);
    lookupTable = zeros(256, 1);
    
    %Put image values into a one dimensinal array
    vArray = reshape(Vbf,[M*N,1]);
    vArray = sort(vArray);
    
    vSegment = round((M*N)/s);
    vIncrement = 1/s;
    x1 = vArray(1);
    x2=0;
    y1 = 0;
    y2=0;
    % Iterate through segments
    for i = 1:s
        nextSeg = i*vSegment;
        if nextSeg > M*N
            nextSeg = M*N;
        end
        x2 = vArray(nextSeg);
        y2 = y2 + vIncrement;
        % Evaluate gradient for function
        if x2-x1 > 0
            m = (y2-y1)/(x2-x1);
        else
            m=0;
        end
        % Evaluate new values by function and put into lookup table
        for j = (i-1)*vSegment+1 : nextSeg
            lookupTable(round(vArray(j)*255)+1) = (y2 - m*(x2-vArray(j)));
        end
        y1 = y2;
        x1 = x2;
    end
    
    V = Vbf;
    % Translate old values into new ones according to table
    for m = 1:M
        for n = 1:N
            V(m,n) = lookupTable(V(m,n)*255+1);
        end
    end
end


