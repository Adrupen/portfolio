function [ V ] = HE( Vbf )
%Histogram Equalization
%   1. Histogram is calculated
%   2. Then the values are spread out over the dynamic range.
%   This is done by mapping values from the original picture to new
%   values. These values are found by the cumulative sum.
%   3. Return the resulting picture

    [M, N] = size(Vbf);
    d = 1.0/(M*N);  %   Determine precent value of one pixel
    h = zeros(256, 1);
    % Iterate through values in image, adding d to the corresponding index
    % in h. h is the histogram.
    for m = 1:M
        for n = 1:N
            h((Vbf(m,n)*255)+1) = h((Vbf(m,n)*255)+1)+d;
        end
    end
    
    sum = 0.0;
    lookup = zeros(256,1);
    % Calculate the cumulative sum
    for i = 1:256
        sum = sum+h(i);
        lookup(i) = sum*256;
    end
    
    V = Vbf;
    % Use the mapping array to convert the old picture to a new one
    for m = 1:M
        for n = 1:N
            V(m,n) = (lookup((V(m,n)*255)+1))/256;
        end
    end

end